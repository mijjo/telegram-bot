<?php
/**
 * README
 * This file is intended to unset the webhook.
 * Uncommented parameters must be filled
 */

// Load composer
require_once __DIR__ . '/vendor/autoload.php';

$settings = @require __DIR__ . "/setting.php";

// Add you bot's API key and name
$bot_api_key  = $settings['bot']['key'];
$bot_username = $settings['bot']['username'];

// Define all IDs of admin users in this array (leave as empty array if not used)
$admin_users = $settings['bot']['admins'];

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

    // Delete webhook
    $result = $telegram->deleteWebhook();

    if ($result->isOk()) {
        echo $result->getDescription();
    }
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e->getMessage();
}
