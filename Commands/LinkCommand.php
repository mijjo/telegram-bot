<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Mijjo\Bot\Controller\UTMBuilder;
use Spatie\Url\Url;

class LinkCommand extends AdminCommand
{
    protected $name = 'link';
    protected $description = 'Create link for campaign';
    protected $usage = '/link';
    protected $version = '1.0.0';
    protected $need_mysql = true;

    protected $link_pattern = '%1$s?utm_source=%2$s&utm_medium=%3$s&utm_campaign=%4$s';

    protected $campaigns = [
        'sources' => [
            'virgool' => [
                'content',
                'image'
            ],
            'sepantam' => [
                'content',
                'banner',
                'author_link'
            ],
            'twitter' => [
                'content',
                'bio'
            ],
            'instagram' => [
                'content',
                'story',
                'bio'
            ]
        ],
        'names' => [
            'authoring',
            'social',
        ]
    ];

    public function execute()
    {
        $message = $this->getMessage();
        $chat    = $message->getChat();
        $chat_id = $chat->getId();
        $user_id = $message->getFrom()->getId();

        $response = [
            'chat_id' => $chat_id,
            'text' => null
        ];

        $conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$conversation->notes;
        !is_array($notes) && $notes = [];

        $state = $notes['state'] ?? 0;

        switch ($state) {
            case 0:
                $this->step0($response, $notes);
                $notes['state'] = 1;
                break;

            case 1:
                $this->step1($response, $notes);
                $notes['state'] = 2;
                break;

            case 2:
                $this->step2($response, $notes);
                $notes['state'] = 3;
                break;

            case 3:
                $this->step3($response, $notes);
                $notes['state'] = 4;
                break;

            case 4:
                $this->step4($response, $notes);
                $conversation->stop();
                break;
        }

        $conversation->update();

        if ($response['text'] === null) {
            return Request::emptyResponse();
        }

        return Request::sendMessage($response);
    }

    protected function step0(&$response, &$notes)
    {
        $response['text'] = 'اول از همه لینکش رو برام بفرست';
    }
    
    protected function step1(&$response, &$notes)
    {
        $selected_url = $this->getMessage()->getText(true);

        if (parse_url($selected_url) === false) {
            $response['text'] = 'آدرس وارد شده صحیح نیست';
            return;
        }
        
        $notes['url'] = preg_replace('/\?.*/', '', $selected_url);

        $sources = array_keys($this->campaigns['sources']);

        $keyboard = new Keyboard(...array_chunk($sources, 2));
        $keyboard->setResizeKeyboard(true)
                    ->setOneTimeKeyboard(false)
                    ->setSelective(false);

        $response['text'] = 'برای کجا میخوایش؟';
        $response['reply_markup'] = $keyboard;
    }

    protected function step2(&$response, &$notes)
    {
        $selected_source = $this->getMessage()->getText(true);
        $notes['source'] = $selected_source;

        if (!isset($this->campaigns['sources'][$selected_source])) {
            $response['text'] = 'لطفا از گزینه های موجود یکی را انتخاب کنید.';
            return;
        }

        $mediums = $this->campaigns['sources'][$selected_source];

        $response['text'] = 'تو چه قسمتی ازش؟';
        $response['reply_markup'] = new Keyboard(...array_chunk($mediums, 2));
    }

    protected function step3(&$response, &$notes)
    {
        $selected_medium = $this->getMessage()->getText(true);
        $notes['medium'] = $selected_medium;

        $names = $this->campaigns['names'];

        $response['text'] = 'از کدوم کمپین؟';
        $response['reply_markup'] = new Keyboard(...array_chunk($names, 2));
    }

    protected function step4(&$response, &$notes)
    {
        $selected_name = $this->getMessage()->getText(true);
        $notes['name'] = $selected_name;

        $url = $notes['url'];
        $source = $notes['source'];
        $medium = $notes['medium'];
        $name = $notes['name'];

        $response['text'] = UTMBuilder::build($url, $source, $medium, $name);
        $response['reply_markup'] = Keyboard::remove();
    }
}
