<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Mijjo\Bot\Controller\UTMBuilder;
use Mijjo\Bot\Helper\MessageHelper;

class UtmCommand extends UserCommand
{

    /**
     * @var string
     */
    protected $description = 'Create utm url';

    /**
     * @var string
     */
    protected $link_pattern = '%1$s?utm_source=%2$s&utm_medium=%3$s&utm_campaign=%4$s';

    /**
     * @var string
     */
    protected $name = 'utm';

    /**
     * @var mixed
     */
    protected $need_mysql = true;

    /**
     * @var array
     */
    protected $presets = [
        '🔵 متنای ویرگول' => [
            'source'   => 'virgool',
            'medium'   => 'content',
            'campaign' => 'social',
        ],
        '🦆 توییتر' => [
            'source'   => 'virgool',
            'medium'   => 'content',
            'campaign' => 'social',
        ],
        'واسه سپنتام'  => [
            'source'   => 'sepantam',
            'medium'   => 'content',
            'campaign' => 'authoring',
        ],
    ];

    /**
     * @var string
     */
    protected $usage = '/utm';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    protected $messages = [
        [
            'لینکی که میگی رو برام بفرست'
        ],
        [
            'لینکش رو واسه کجا میخوای؟',
            'برای کجا میخواییش؟'
        ],
        [
            'اینم خدمت شما',
            'بفرما 😉',
        ]
    ];

    public function execute()
    {
        $message = $this->getMessage();
        $chat    = $message->getChat();
        $chat_id = $chat->getId();
        $user_id = $message->getFrom()->getId();

        $response = [
            'chat_id' => $chat_id,
            'text'    => null,
        ];

        $conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$conversation->notes;
        !is_array($notes) && $notes = [];

        $state = $notes['state'] ?? 0;

        switch ($state) {
            case 0:
                $response['text'] = MessageHelper::choose($this->messages[0]);
                $response['reply_markup'] = Keyboard::remove();
                Request::sendMessage($response);

                $notes['state'] = 2;
                $conversation->update();
            break;

            case 1:
              /*   if(!UTMBuilder::isUrl($message)) {
                    $response['text'] = 'گفتم لینک بفرست.';
                    Request::sendMessage($response);

                    $response['text'] = 'اینی که فرستادی لینک نیست';
                    Request::sendMessage($response);
                    break;
                } */

                $keyboard = new Keyboard(...array_keys($this->presets));
                $keyboard = $keyboard->setResizeKeyboard(true);

                $response['text'] = MessageHelper::choose($this->messages[1]);
                $response['reply_markup'] = $keyboard;
                Request::sendMessage($response);

                $notes['url'] = $message->getText();
                $notes['state'] = 2;
                $conversation->update();
            break;

            case 2:
                if (!in_array($message->getText(true), array_keys($this->presets))) {
                    $response['text'] = 'متوجه نمیشم لطفا از کیبورد استفاده کن';
                    Request::sendMessage($response);
                    break;
                }

                $response['text'] = MessageHelper::choose($this->messages[2]);
                Request::sendMessage($response);
                
                $params = $this->presets[$message->getText()];
                $url = $notes['url'];
                $source = $params['source'];
                $medium = $params['medium'];
                $campaign = $params['campaign'];

                $result = UTMBuilder::build($url, $source, $medium, $campaign);
                $response['text'] = $result;
                $response['reply_markup'] = Keyboard::remove();
                Request::sendMessage($response);
                $conversation->stop();
            break;

            default:
                $conversation->cancel();
                $response['text'] = MessageHelper::$error_message;
                break;
        }

        return Request::emptyResponse();

    }
}
