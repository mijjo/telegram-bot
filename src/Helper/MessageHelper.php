<?php namespace Mijjo\Bot\Helper;

/**
 * @property-read string $error_message
 */
class MessageHelper
{
    private static $error_messages = [
        [
            'متاسفم. فکر کنم یک اشتباهی پیش اومده'
        ],

    ];
    
    public static function choose($messages)
    {
        $length = count($messages);

        if ($length > 0) {
            $randomIndex = random_int(0, $length - 1);
            $choosen     = $messages[$randomIndex];

            return $choosen;
        }

        return $messages;
    }

    public static function __get($param)
    {
        if($param === 'error_message') {
            return static::choose(self::$error_messages[0]);
        }
        
        return null;
    }
}

