<?php
namespace Mijjo\Bot\Controller;

class UTMBuilder
{
    /**
     * @param $url
     * @param $source
     * @param $medium
     * @param $campaign
     * @param null $term
     * @param null $content
     */
    public static function build($url, $source, $medium, $campaign, $term = null, $content = null)
    {
        $params = [
            'utm_source'   => $source,
            'utm_medium'   => $medium,
            'utm_campaign' => $campaign,
            'utm_term'     => $term,
            'utm_content'  => $content,
        ];

        $url    = self::stripUrl($url);
        $result = $url . '?';
        $index  = 0;

        foreach ($params as $key => $value) {
            if ($value !== null) {
                $result .= $index > 0 ? '&' : '';

                $result .= "{$key}={$value}";
                $index++;
            }
        }

        return $result;
    }

    /**
     * @param $url
     */
    public static function isUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function stripUrl($url)
    {
        $result = preg_replace('/\?.*/', '', $url);

        return $result;
    }
}
